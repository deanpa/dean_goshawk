#!/usr/bin/env python

import os
import numpy as np
#from numba import jit
#import pylab as P
#import pandas as pd


def decodeBytes(byteArray, nArray):
    """
    ## loop to decode byte to string
    """
    strArray = str(byteArray[0], 'utf-8')
    for i in range(1, nArray):
        strArr_i = str(byteArray[i], 'utf-8')
        strArray = np.append(strArray, strArr_i)
    return(strArray)


def distFX(x1, y1, x2, y2):
    """
    calc distance matrix between 2 sets of points
    resulting matrix dimensions = (len(x1), len(x2))
    """
    deltaX = np.add.outer(x1, -x2)
    deltaX_sq = deltaX**2
    deltaY = np.add.outer(y1, -y2)
    deltaY_sq = deltaY**2
    dist_2D = np.sqrt(deltaX_sq + deltaY_sq)
    return dist_2D


class LocData(object):
    """
    read and get data
    """
    def __init__(self, eastFname, knownFname, eastlocationFname):

        ## run functions  #######################

        self.readKnownNests(knownFname)
        self.getNestID()
        self.getEastKnown()
        self.findEastAlternateNests()
        self.removeDups()
        self.readEastNests(eastFname)
        self.makeEmptyEastArrays()
        self.getEastRandomLoc()
        self.writeEastToFile(eastlocationFname)
        ## end running functions  ###############


    def readKnownNests(self, knownFname):
        """
        ## read in data for known nests
        """
        knownNestDat = np.genfromtxt(knownFname,  delimiter=',', names=True,
            dtype=['S10', 'S10', 'f8', 'f8', 'i8', 'S10', 'i8'])
        self.knownZone = knownNestDat['zone']
        self.knownNestID = knownNestDat['nestID']
        self.knownY = knownNestDat['y']
        self.knownX = knownNestDat['x']
        self.knownYear = knownNestDat['year']
        self.knownStatus = knownNestDat['status']
        self.knownPA = knownNestDat['pa']
        self.nKnown = len(self.knownX)
        ## convert bytes to string and format Dates
        self.knownZone = decodeBytes(self.knownZone, self.nKnown)
        self.knownZoneNo = np.where(self.knownZone == 'east', 1, 0)
        self.knownStatus = decodeBytes(self.knownStatus, self.nKnown)
        self.knownStatusNo = np.zeros(self.nKnown, dtype = int)
        self.knownStatusNo[self.knownStatus == 'Active'] = 1
        self.knownStatusNo[self.knownStatus == 'Unknown'] = 2
        self.knownNestID = decodeBytes(self.knownNestID, self.nKnown)
        self.uYears = np.unique(self.knownYear)
        self.nYears = len(self.uYears)
        print('uYears', self.uYears)

        ######################################################################
        ###  MISSING Y DATA FOR NEST DE038 
###        maskTemp = self.knownNestID == 'DE038'
###        print('DE038', 'x', self.knownX[maskTemp], 'y', self.knownY[maskTemp])
        ###
        ######################################################################

#        print(self.knownZone[:5], self.knownNestID[:5], self.knownY[:5], self.knownYear[:5], 
#                self.knownPA[:5], self.knownStatusNo[:5], self.nKnown)

    def getNestID(self):
        """
        convert known nests id to integers
        """
        self.knownNestNo = np.zeros(self.nKnown, dtype = int)
        uniqueNest = np.unique(self.knownNestID)
#        print('unique nest', uniqueNest)
        self.nUniqueKnownNest = len(uniqueNest)
        for i in range(self.nUniqueKnownNest):
            nest_i = uniqueNest[i]
            mask_i = self.knownNestID == nest_i
            self.knownNestNo[mask_i] = i
#        print(self.knownNestID[:50], self.knownNestNo[:50], self.nUniqueKnownNest)

    def getEastKnown(self):
        """
        get known nests in the east zone
        """
        maskE = self.knownZoneNo == 1
        self.knownEZone = self.knownZoneNo[maskE]
        self.knownEStatus = self.knownStatusNo[maskE]
        self.knownENest = self.knownNestNo[maskE]
        self.knownENestID = self.knownNestID[maskE]
        self.knownEX = self.knownX[maskE]
        self.knownEY = self.knownY[maskE]
        self.knownEPA = self.knownPA[maskE]
        self.knownEYear = self.knownYear[maskE]
        self.nKnownEast = len(self.knownEPA)
        self.uYearsEast = np.unique(self.knownEYear)
        self.nYearsEast = len(self.uYearsEast)
        print('uYearsEast', self.uYearsEast)

    def findEastAlternateNests(self):
        """
        aggregate alternate nests into a single master nest
        """
        pres1Mask = self.knownEPA == 1
        keepMask = np.zeros(self.nKnownEast, dtype = bool)
        masterNest = self.knownENest.copy()
        ## loop unique nests to get 1 entry of location data for each nest
        uNestsArr = np.unique(self.knownENest)
        nUEastNests = len(uNestsArr)
        uX = np.zeros(nUEastNests)
        uY = np.zeros(nUEastNests)
        for i in range(nUEastNests):
            nest_i = uNestsArr[i]
            mask_i = self.knownENest == nest_i
            uX[i] = self.knownEX[mask_i][0]
            uY[i] = self.knownEY[mask_i][0]
        # loop unique nests
        # for each new unique nest, find nests <= 400 m
        # among these, select one per year, unless two are occupied. 
        # If two are occupied, raise error.
        # always select occupied nest.
        uNests = uNestsArr.copy()
        while len(uNests) > 0:
            ## get x y of nest
            nest_i = uNests[0]
            
            maskNest_i = self.knownENest == nest_i
            x_i = uX[0]            
            y_i = uY[0]
            ## get all other x y data
            x_others = uX[1:] 
            y_others = uY[1:] 
            dist = distFX(x_i, y_i, x_others, y_others)
            maskDist = dist <= 400.0
            ## if nest i is isolated by at least 400 m
            if sum(maskDist) == 0:
                keepMask[maskNest_i] = True
                ## update uNests and x y
                uNests = uNests[1:]
                uX = uX[1:]
                uY = uY[1:]
                continue
            ## IF NEST HAS ALTERNATIVES    
            aggrNests = uNests[1:][maskDist]
            # Resolve which to keep and when.
            aggrMask = np.append(nest_i, aggrNests)
            fullNestMask = np.in1d(self.knownENest, aggrMask)
            uYears = np.unique(self.knownEYear[fullNestMask])
            for j in range(len(uYears)):
                year_j = uYears[j]
                fullYearMask = self.knownEYear == year_j
                nestYearMask = fullNestMask & fullYearMask
                pa_j = self.knownEPA[nestYearMask]
                ## IF MORE THAN 1 PRESENCE                                        
                if np.sum(pa_j) > 1:
                    paNestYearMask = pres1Mask & nestYearMask
                    print('more than 1 pres, year =', year_j, 
                            'nests = ', self.knownENest[paNestYearMask])
                    keepMask[paNestYearMask] = True
                    masterNest[paNestYearMask] = nest_i
                ## ONLY 1 PRESENCE
                elif np.sum(pa_j) == 1:
                    paNestYearMask = pres1Mask & nestYearMask
                    keepMask[paNestYearMask] = True
                    masterNest[paNestYearMask] = nest_i
                ## NO PRESENCES AMOUNG ALL NESTS
                elif np.sum(pa_j) == 0:
                    randNest = np.random.choice(aggrNests, 1)
                    keep1NestMask = self.knownENest == randNest
                    keepNestYearMask = keep1NestMask & fullYearMask
                    keepMask[keepNestYearMask] = True
                    masterNest[keepNestYearMask] = nest_i
            ## update uNests and x y
            rmMask = ~np.in1d(uNests, aggrMask)
            uNests = uNests[rmMask]
            uX = uX[rmMask]
            uY = uY[rmMask]
        self.knownEZone = self.knownEZone[keepMask]
        self.knownEStatus = self.knownEStatus[keepMask]
        self.knownENest = self.knownENest[keepMask]
        self.knownENestID = self.knownENestID[keepMask]
        self.knownMasterNest = masterNest[keepMask]
        self.knownEX = self.knownEX[keepMask]
        self.knownEY = self.knownEY[keepMask]
        self.knownEPA = self.knownEPA[keepMask]
        self.knownEYear = self.knownEYear[keepMask]
        self.nKnownEast = len(self.knownEPA)
        self.uYearsEast = np.unique(self.knownEYear)
        self.nYearsEast = len(self.uYearsEast)
        print('uYearsEast', self.uYearsEast)


    def removeDups(self):
        """
        remove repeat entries for nests with > 1 entry per year
        """
        pres1Mask = self.knownEPA == 1
        remMask = np.zeros(self.nKnownEast, dtype = bool)
        uNests = np.unique(self.knownMasterNest)
        nUNests = len(uNests)
        for i in range(nUNests):
            nest_i = uNests[i]
            nestMask = self.knownMasterNest == nest_i
            uYears_i = np.unique(self.knownEYear[nestMask])
            nUYears_i = len(uYears_i)

            for j in range(nUYears_i):
                year_j = uYears_i[j]
                yearMask = self.knownEYear == year_j
                nestYearMask = yearMask & nestMask

    
                if nest_i == 0:
                    print('nest', nest_i, 'knownMasterNest', self.knownMasterNest[nestYearMask],
                        'years', self.knownEYear[nestYearMask], 
                        'sum mask', np.sum(nestYearMask)) 




                if np.sum(nestYearMask) > 1:
                    ## make all entries to be removed; make one to keep below
                    remMask[nestYearMask] = True
                    pa_j = self.knownEPA[nestYearMask]
                    nPres = np.sum(pa_j)
                    ## IF 1 or MORE PRESENCE, then keep first                                        
                    if nPres > 0:
                        paNestYearMask = pres1Mask & nestYearMask
                        datumRemoved = False
                        ## LOOP THRU ALL ENTRIES
                        for k in range(self.nKnownEast):
                            if paNestYearMask[k] & (not datumRemoved):
                                remMask[k] = False
                                datumRemoved = True

                        print('2nd Filter more than 1 pres, year =', year_j, 
                            'nests = ', nest_i, 'pa_j', pa_j,
                            'remmask', remMask[nestYearMask])

        
#                            if nest_i == 0:
#                                print('nest', nest_i, 'year', year_j, 'pa_j', pa_j) 


                    ## IF ALL ARE ABSENT
                    elif nPres == 0:
                        datumRemoved = False
                        ## LOOP THRU ALL ENTRIES
                        for k in range(self.nKnownEast):
                            if nestYearMask[k] & (not datumRemoved):
                                remMask[k] = False
                                datumRemoved = True

                        print('2nd Filter more than 1 pres, year =', year_j, 
                            'nests = ', nest_i, 'pa_j', pa_j,
                            'remmask', remMask[nestYearMask])
        ## REMOVE ENTRIES FROM ARRAYS
        self.knownEZone = self.knownEZone[~remMask]
        self.knownEStatus = self.knownEStatus[~remMask]
        self.knownENest = self.knownENest[~remMask]
        self.knownENestID = self.knownENestID[~remMask]
        self.knownMasterNest = self.knownMasterNest[~remMask]
        self.knownEX = self.knownEX[~remMask]
        self.knownEY = self.knownEY[~remMask]
        self.knownEPA = self.knownEPA[~remMask]
        self.knownEYear = self.knownEYear[~remMask]
        self.nKnownEast = len(self.knownEPA)


    def readEastNests(self, eastFname):
        """
        read in data from east polygon centroids - absences.
        """
        eastNestDat = np.genfromtxt(eastFname,  delimiter=',', names=True,
            dtype=['f8', 'f8', 'i8', 'S32', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8'])
#        self.eastObjectID = eastNestDat['objectid']
#        self.eastStandID = eastNestDat['standID']
        self.eastY = eastNestDat['y']
        self.eastX = eastNestDat['x']
#        self.eastPolyID = eastNestDat['polyID']
#        self.eastDistrictN = eastNestDat['districtN']
#        self.eastLocation = eastNestDat['location']
        self.year0 = eastNestDat['y0']
        self.year1 = eastNestDat['y1']
        self.year2 = eastNestDat['y2']
        self.year3 = eastNestDat['y3']

        self.nEast = len(self.eastX)
        ## convert bytes to string and format Dates
#        self.eastStandID = decodeBytes(self.eastStandID, self.nEast)
#        print(self.eastY[:5], self.eastX[:5], self.eastStandID[:5], self.nEast)
        self.eastYearSurveyArr = np.zeros((self.nEast, 4), dtype = int)
        self.eastYearSurveyArr[:, 0] = self.year0
        self.eastYearSurveyArr[:, 1] = self.year1
        self.eastYearSurveyArr[:, 2] = self.year2
        self.eastYearSurveyArr[:, 3] = self.year3


    def makeEmptyEastArrays(self):
        nTotal = (self.nYears * self.nEast) + self.nKnownEast
        self.Zone = np.ones(nTotal, dtype = int)        # all will be 1 for east
        self.Nest = np.zeros(nTotal, dtype = int) 
        self.NestID = np.repeat('Random', nTotal) 
        self.MasterNest = np.zeros(nTotal, dtype = int) 
        self.Y = np.zeros(nTotal)
        self.X = np.zeros(nTotal)
        self.Year = np.zeros(nTotal, dtype = int)
        self.Status = np.zeros(nTotal, dtype = int)
        self.PA = np.zeros(nTotal, dtype = int)
        self.Random = np.ones(nTotal, dtype = int)
        print('ntotal', nTotal)
        # populate with known data
        self.Zone[:self.nKnownEast] = self.knownEZone
        self.Nest[:self.nKnownEast] = self.knownENest
        self.NestID[:self.nKnownEast] = self.knownENestID
        self.MasterNest[:self.nKnownEast] = self.knownMasterNest
        self.Y[:self.nKnownEast] = self.knownEY
        self.X[:self.nKnownEast] = self.knownEX
        self.Year[:self.nKnownEast] = self.knownEYear
        self.Status[:self.nKnownEast] = self.knownEStatus
        self.PA[:self.nKnownEast] = self.knownEPA
        self.Random[:self.nKnownEast] = 0

    def getEastRandomLoc(self):
        cc = self.nKnownEast        # counter to populate arrays
        newNestNo_CC = self.nUniqueKnownNest 
        # loop years
        for i in range(self.nYearsEast):
            rand_i = np.random.permutation(self.nEast)
            year_i = self.uYearsEast[i]
            ## loop thru potential random locations
            for j in range(self.nEast):
                # get random location j
                rand_ij = rand_i[j]
                ## if this point was not surveyed in year i, then continue to next point
                pointSurveyed = np.in1d(year_i, self.eastYearSurveyArr[rand_ij])
                if not pointSurveyed:
                    continue
                ## get all populated cells up to this point for Year i
                yrMask = self.Year == year_i
                x_i = self.X[yrMask]
                y_i = self.Y[yrMask]
                x_j = self.eastX[rand_ij]
                y_j = self.eastY[rand_ij]
                dist = distFX(x_j, y_j, x_i, y_i)

                if len(dist) == 0:
                    print('No distance', i, j)
                ## if keep location, populate the arrays
                if np.min(dist) >= 1000.0:      
                    self.Nest[cc] = newNestNo_CC
                    self.X[cc] = x_j        
                    self.Y[cc] = y_j
                    self.Year[cc] = year_i
                    # status, pa and zone are [0, 0, 1] for all entries
                    cc += 1
                    newNestNo_CC += 1            
        ### cull off all unused rows
        maskKeep = self.X > 0.0
        self.Zone = self.Zone[maskKeep]
        self.Nest = self.Nest[maskKeep]
        self.NestID = self.NestID[maskKeep]
        self.MasterNest = self.MasterNest[maskKeep]
        self.Y = self.Y[maskKeep]
        self.X = self.X[maskKeep]
        self.Year = self.Year[maskKeep]
        self.Status = self.Status[maskKeep]
        self.PA = self.PA[maskKeep]
        self.nEastData = len(self.PA)
        self.Random = self.Random[maskKeep]

    def writeEastToFile(self, eastlocationFname):
        """
        # Write result table to file
        """
        # create new structured array with columns of different types
        structured = np.empty((self.nEastData,), dtype=[('zone', 'i8'), ('nest', 'i8'), 
                    ('nestid', 'U8'), ('masternest', 'i8'), ('x', np.float),
                    ('y', np.float), ('year', 'i8'), ('status', 'i8'), 
                    ('pa', 'i8'), ('random', 'i8')])
        # copy data over
        structured['zone'] = self.Zone
        structured['nest'] = self.Nest
        structured['nestid'] = self.NestID
        structured['masternest'] = self.MasterNest
        structured['y'] = self.Y
        structured['x'] = self.X
        structured['year'] = self.Year
        structured['status'] = self.Status
        structured['pa'] = self.PA
        structured['random'] = self.Random
        fmts = ['%i', '%i', '%s', '%i', '%.2f', '%.2f', '%i', '%i', '%i', '%i']
        np.savetxt(eastlocationFname, structured, comments = '', delimiter = ',', fmt = fmts,
                    header='zone, nest, NestID, MasterNest, x, y, year, status, pa, random')


def main():
    # set paths to scripts and data
    inputDataPath = os.path.join(os.getenv('KIWIPROJDIR', default='.'), 'goshawk_data')

    ## set Data names
#    eastFname = os.path.join(inputDataPath, 'EastCentroidPts.csv')
    eastFname = os.path.join(inputDataPath, 'EastCentroidPtsYears.csv')
    knownFname = os.path.join(inputDataPath, 'knownEWNestsAllYears.csv') 
    eastlocationFname = os.path.join(inputDataPath, 'eastLocationData_3.csv')
    
    locdata = LocData(eastFname, knownFname, eastlocationFname)


if __name__ == '__main__':
    main()
