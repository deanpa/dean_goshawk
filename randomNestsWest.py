#!/usr/bin/env python

import os
import numpy as np
#from numba import jit
#import pylab as P
#import pandas as pd


def decodeBytes(byteArray, nArray):
    """
    ## loop to decode byte to string
    """
    strArray = str(byteArray[0], 'utf-8')
    for i in range(1, nArray):
        strArr_i = str(byteArray[i], 'utf-8')
        strArray = np.append(strArray, strArr_i)
    return(strArray)


def distFX(x1, y1, x2, y2):
    """
    calc distance matrix between 2 sets of points
    resulting matrix dimensions = (len(x1), len(x2))
    """
    deltaX = np.add.outer(x1, -x2)
    deltaX_sq = deltaX**2
    deltaY = np.add.outer(y1, -y2)
    deltaY_sq = deltaY**2
    dist_2D = np.sqrt(deltaX_sq + deltaY_sq)
    return dist_2D


class LocData(object):
    """
    read and get data
    """
    def __init__(self, westFname, knownFname, westlocationFname):

        ## run functions  #######################

        self.readKnownNests(knownFname)
        self.getWestKnown()
        self.getNestID()
        self.removeDups()
        self.findWestAlternateNests()
        self.readWestNests(westFname)
        self.makeEmptyWestArrays()
        self.getWestRandomLoc()
        self.writeWestToFile(westlocationFname)
        ## end running functions  ###############


    def readKnownNests(self, knownFname):
        """
        ## read in data for known nests
        """
        knownNestDat = np.genfromtxt(knownFname,  delimiter=',', names=True,
            dtype=['S10', 'S10', 'f8', 'f8', 'i8', 'S10', 'i8'])
        self.knownZone = knownNestDat['zone']
        self.knownNestID = knownNestDat['nestID']
        self.knownY = knownNestDat['y']
        self.knownX = knownNestDat['x']
        self.knownYear = knownNestDat['year']
        self.knownStatus = knownNestDat['status']
        self.knownPA = knownNestDat['pa']
        self.nKnown = len(self.knownX)
        ## convert bytes to string and format Dates
        self.knownZone = decodeBytes(self.knownZone, self.nKnown)
        self.knownZoneNo = np.where(self.knownZone == 'west', 0, 1)
        self.knownStatus = decodeBytes(self.knownStatus, self.nKnown)
        self.knownStatusNo = np.zeros(self.nKnown, dtype = int)
        self.knownStatusNo[self.knownStatus == 'Active'] = 1
        self.knownStatusNo[self.knownStatus == 'Unknown'] = 2
        self.knownNestID = decodeBytes(self.knownNestID, self.nKnown)
        self.uYears = np.unique(self.knownYear)
        self.nYears = len(self.uYears)
        print('uYears', self.uYears)

#        ######################################################################
#        ###  MISSING Y DATA FOR NEST DE038 
#        maskTemp = self.knownNestID == 'DE038'
#        print('DE038', 'x', self.knownX[maskTemp], 'y', self.knownY[maskTemp])
#        ###
#        ######################################################################

#        print(self.knownZone[:5], self.knownNestID[:5], self.knownY[:5], self.knownYear[:5], 
#                self.knownPA[:5], self.knownStatusNo[:5], self.nKnown)

    def getWestKnown(self):
        """
        get known nests in the West zone
        """
        maskW = self.knownZoneNo == 0
        self.knownWZone = self.knownZoneNo[maskW]
        self.knownWStatus = self.knownStatusNo[maskW]
        self.knownWNestID = self.knownNestID[maskW]
        self.knownWX = self.knownX[maskW]
        self.knownWY = self.knownY[maskW]
        self.knownWPA = self.knownPA[maskW]
        self.knownWYear = self.knownYear[maskW]
        self.nKnownWest = len(self.knownWPA)
        self.uYearsWest = np.unique(self.knownWYear)
        self.nYearsWest = len(self.uYearsWest)


    def getNestID(self):
        """
        convert known nests id to integers
        """
        self.knownWNest = np.zeros(self.nKnownWest, dtype=int)
#        self.knownNestNo = np.zeros(self.nKnown, dtype = int)
        uniqueNest = np.unique(self.knownWNestID)
#        print('unique nest', uniqueNest)
        self.nUniqueKnownNest = len(uniqueNest)
        for i in range(self.nUniqueKnownNest):
            nest_i = uniqueNest[i]
            mask_i = self.knownWNestID == nest_i
            self.knownWNest[mask_i] = i
            print('nest_i', nest_i, 'i', i)
#        print(self.knownNestID[:50], self.knownNestNo[:50], self.nUniqueKnownNest)



    def removeDups(self):
        """
        ## NEED TO REMOVE DUPLICATES FOR NEST_I
        """

#        for i in range(self.nKnownWest):
#            print('nest', self.knownWNest[i], 'id', self.knownWNestID[i], 'pa', self.knownWPA[i],
#                'year', self.knownWYear[i])


        pa1Mask = self.knownWPA == 1
        keepMask = np.zeros(len(self.knownWY), dtype = bool)
        ## loop unique nests to get 1 entry of location data for each nest-year combo
        uNestsArr = np.unique(self.knownWNest)
        nUWestNests = len(uNestsArr)
        for i in range(nUWestNests):
            nestMask_i = self.knownWNest == uNestsArr[i]
            uYear_i = np.unique(self.knownWYear[nestMask_i])
            for j in range(len(uYear_i)):
                yrMask = self.knownWYear == uYear_i[j]
                nestYearMask = yrMask & nestMask_i
                ## number of year entries for nest i and year j
                nYrDat = np.sum(nestYearMask)
                ## if more than one entry for year j
                if nYrDat > 1:
                    pa_ij = self.knownWPA[nestYearMask]
                    # if only 1 positive for year j, keep it
                    if (np.sum(pa_ij) == 1):
                        nestYearPA1Mask = pa1Mask & nestYearMask
                        keepMask[nestYearPA1Mask] = True
                    # if more than 1 positive for year j, k
                    elif (np.sum(pa_ij) > 1):
                        nestYearPA1Mask = pa1Mask & nestYearMask
                        lenPA1 = np.sum(nestYearPA1Mask)
                        fillArr = np.zeros(lenPA1)
                        fillArr[0] = 1
                        keepMask[nestYearPA1Mask] = fillArr
                    elif (np.sum(pa_ij) == 0):
                        fillArr = np.zeros(nYrDat)
                        fillArr[0] = 1
                        keepMask[nestYearMask] = fillArr
                elif nYrDat == 1:
                    keepMask[nestYearMask] = 1 
        self.knownWZone = self.knownWZone[keepMask]
        self.knownWStatus = self.knownWStatus[keepMask]
        self.knownWNest = self.knownWNest[keepMask]
        self.knownWNestID = self.knownWNestID[keepMask]
        self.knownWX = self.knownWX[keepMask]
        self.knownWY = self.knownWY[keepMask]
        self.knownWPA = self.knownWPA[keepMask]
        self.knownWYear = self.knownWYear[keepMask]
        self.nKnownWest = len(self.knownWPA)
        self.uYearsWest = np.unique(self.knownWYear)
        self.nYearsWest = len(self.uYearsWest)
#        print('uYearsWest known, remove dups', self.uYearsWest)

        for i in range(self.nKnownWest):
            print('nest', self.knownWNest[i], 'id', self.knownWNestID[i], 'pa', self.knownWPA[i],
                'year', self.knownWYear[i])


            


    def findWestAlternateNests(self):
        """
        aggregate alternate nests into a single master nest
        """
        pres1Mask = self.knownWPA == 1
        keepMask = np.zeros(self.nKnownWest, dtype = bool)
        masterNest = self.knownWNest.copy()
        ## loop unique nests to get 1 entry of location data for each nest
        uNestsArr = np.unique(self.knownWNest)
        nUWestNests = len(uNestsArr)
        uX = np.zeros(nUWestNests)
        uY = np.zeros(nUWestNests)
        for i in range(nUWestNests):
            nest_i = uNestsArr[i]
            mask_i = self.knownWNest == nest_i
            uX[i] = self.knownWX[mask_i][0]
            uY[i] = self.knownWY[mask_i][0]
        # loop unique nests
        # for each new unique nest, find nests <= 400 m
        # among these, select one per year, unless two are occupied. If two are occupied, raise error.
        # always select occupied nest.
        uNests = uNestsArr.copy()
        while len(uNests) > 0:
            ## get x y of nest
            nest_i = uNests[0]            
            maskNest_i = self.knownWNest == nest_i
            x_i = uX[0]            
            y_i = uY[0]
            ## get all other x y data
            x_others = uX[1:] 
            y_others = uY[1:] 
            dist = distFX(x_i, y_i, x_others, y_others)
#            print('nest_i', nest_i, 'x', x_i, 'y', y_i)            #, 'x_others', x_others)
            maskDist = dist <= 400.0
            ## if nest i is isolated by at least 400 m
            if sum(maskDist) == 0:
                keepMask[maskNest_i] = True
                ## update uNests and x y
                uNests = uNests[1:]
                uX = uX[1:]
                uY = uY[1:]
                continue
            ## IF NEST HAS ALTERNATIVES    
            aggrNest = uNests[1:][maskDist]
            # Resolve which to keep and when.
            aggrNest = np.append(nest_i, aggrNest)
            fullNestMask = np.in1d(self.knownWNest, aggrNest)
            uYears = np.unique(self.knownWYear[fullNestMask])

            print('aggrNest', aggrNest, 'uyears', uYears)

            for j in range(len(uYears)):
                year_j = uYears[j]
                fullYearMask = self.knownWYear == year_j
                nestYearMask = fullNestMask & fullYearMask
                pa_j = self.knownWPA[nestYearMask]
                
                print('year_j', year_j, 'pa_j', pa_j)
                ## ONLY ONE ENTRY FOR THIS YEAR
                if len(pa_j) == 1:
                    keepMask[nestYearMask] = True
                    masterNest[nestYearMask] = nest_i
                ## MORE THAN ONE ENTRY FOR YEAR
                elif len(pa_j) > 1:
                    ## IF MORE THAN 1 PRESENCE                                        
                    if np.sum(pa_j) > 1:
                        paNestYearMask = pres1Mask & nestYearMask
                        print('more than 1 pres, year =', year_j, 
                            'nests = ', self.knownWNest[paNestYearMask])

                        lenPA1 = np.sum(paNestYearMask)
                        fillArr = np.zeros(lenPA1)
                        fillArr[0] = 1
                        keepMask[paNestYearMask] = fillArr
                        masterNest[paNestYearMask] = nest_i
                    ## ONLY 1 PRESENCE
                    elif np.sum(pa_j) == 1:
                        paNestYearMask = pres1Mask & nestYearMask
                        keepMask[paNestYearMask] = True
                        masterNest[paNestYearMask] = nest_i
                    ## NO PRESENCES AMONG ALL NESTS
                    elif np.sum(pa_j) == 0:
                        nestPool = self.knownWNest[nestYearMask]
                        nPool = len(nestPool)
                        indx = np.random.choice(nPool)
                        randNest = nestPool[indx]
                        keep1NestMask = self.knownWNest == randNest
                        keepNestYearMask = keep1NestMask & fullYearMask
                        keepMask[keepNestYearMask] = True
                        masterNest[keepNestYearMask] = nest_i
            ## update uNests and x y
            rmMask = ~np.in1d(uNests, aggrNest)
            uNests = uNests[rmMask]
            uX = uX[rmMask]
            uY = uY[rmMask]
        self.knownWZone = self.knownWZone[keepMask]
        self.knownWStatus = self.knownWStatus[keepMask]
        self.knownWNest = self.knownWNest[keepMask]
        self.knownWNestID = self.knownWNestID[keepMask]
        self.knownMasterNest = masterNest[keepMask]
        self.knownWX = self.knownWX[keepMask]
        self.knownWY = self.knownWY[keepMask]
        self.knownWPA = self.knownWPA[keepMask]
        self.knownWYear = self.knownWYear[keepMask]
        self.nKnownWest = len(self.knownWPA)
        self.uYearsWest = np.unique(self.knownWYear)
        self.nYearsWest = len(self.uYearsWest)
        print('uYearsWest known', self.uYearsWest)



    def readWestNests(self, westFname):
        """
        read in data from west polygon centroids - absences.
        """
        westNestDat = np.genfromtxt(westFname,  delimiter=',', names=True,
            dtype=['S32', 'i8', 'f8', 'f8'])
###        westNestDat = np.genfromtxt(westFname,  delimiter=',', names=True,
###            dtype=['f8', 'f8', 'i8', 'S32', 'f8', 'i8', 'f8', 'f8', 'i8'])
#        self.westObjectID = westNestDat['objectid']
#        self.westStandID = westNestDat['standID']
        self.westY = westNestDat['POINT_Y']
        self.westX = westNestDat['POINT_X']
        self.westYear = westNestDat['NOGO_Surv']
        self.nWest = len(self.westX)
#        self.westStandID = decodeBytes(self.westStandID, self.nWest)
#        self.eastPolyID = eastNestDat['polyID']
#        self.eastDistrictN = eastNestDat['districtN']
#        self.eastLocation = eastNestDat['location']
#        self.year0 = westNestDat['y0']
#        self.year1 = westNestDat['y1']
#        self.year2 = westNestDat['y2']
#        self.year3 = westNestDat['y3']
        ## convert bytes to string and format Dates
#        print(self.westY[:5], self.westX[:5], self.westStandID[:5], self.nWest)
#        self.westYearSurveyArr = np.zeros((self.nWest, 4), dtype = int)
#        self.westYearSurveyArr[:, 0] = self.year0
#        self.westYearSurveyArr[:, 1] = self.year1
#        self.westYearSurveyArr[:, 2] = self.year2
#        self.westYearSurveyArr[:, 3] = self.year3
        print('rand year', np.unique(self.westYear))


    def makeEmptyWestArrays(self):
        nTotal = (self.nYearsWest * self.nWest) + self.nKnownWest
        self.Zone = np.ones(nTotal, dtype = int)        # all will be 1 for west
        self.Nest = np.zeros(nTotal, dtype = int) 
        self.NestID = np.repeat('Random', nTotal) 
        self.MasterNest = np.zeros(nTotal, dtype = int) 
        self.Y = np.zeros(nTotal)
        self.X = np.zeros(nTotal)
        self.Year = np.zeros(nTotal, dtype = int)
        self.Status = np.zeros(nTotal, dtype = int)
        self.PA = np.zeros(nTotal, dtype = int)
        self.Random = np.ones(nTotal, dtype = int)
        print('ntotal', nTotal)
        # populate with known data
        self.Zone[:self.nKnownWest] = self.knownWZone
        self.Nest[:self.nKnownWest] = self.knownWNest
        self.NestID[:self.nKnownWest] = self.knownWNestID
        self.MasterNest[:self.nKnownWest] = self.knownMasterNest
        self.Y[:self.nKnownWest] = self.knownWY
        self.X[:self.nKnownWest] = self.knownWX
        self.Year[:self.nKnownWest] = self.knownWYear
        self.Status[:self.nKnownWest] = self.knownWStatus
        self.PA[:self.nKnownWest] = self.knownWPA
        self.Random[:self.nKnownWest] = 0

    def getWestRandomLoc(self):
        cc = self.nKnownWest        # counter to populate arrays
        newNestNo_CC = self.nUniqueKnownNest 
        # loop years
        for i in range(self.nYearsWest):
            rand_i = np.random.permutation(self.nWest)
            year_i = self.uYearsWest[i]
            ## loop thru potential random locations
            for j in range(self.nWest):
                # get random location j
                rand_ij = rand_i[j]
                ## if this point was not surveyed in year i, then continue to next point
                pointSurveyed = year_i == self.westYear[rand_ij]
#                pointSurveyed = np.in1d(year_i, self.westYearSurveyArr[rand_ij])
                if not pointSurveyed:
                    continue
                ## get all populated cells up to this point for Year i
                yrMask = self.Year == year_i
                x_i = self.X[yrMask]
                y_i = self.Y[yrMask]
                x_j = self.westX[rand_ij]
                y_j = self.westY[rand_ij]
                dist = distFX(x_j, y_j, x_i, y_i)

                if len(dist) == 0:
                    print('No distance', i, j)
                ## if keep location, populate the arrays
                if np.min(dist) >= 1000.0:      
                    self.Nest[cc] = newNestNo_CC
                    self.X[cc] = x_j        
                    self.Y[cc] = y_j
                    self.Year[cc] = year_i
                    # status, pa and zone are [0, 0, 1] for all entries
                    cc += 1
                    newNestNo_CC += 1            
        ### cull off all unused rows
        maskKeep = self.X > 0.0
        self.Zone = self.Zone[maskKeep]
        self.Nest = self.Nest[maskKeep]
        self.NestID = self.NestID[maskKeep]
        self.MasterNest = self.MasterNest[maskKeep]
        self.Y = self.Y[maskKeep]
        self.X = self.X[maskKeep]
        self.Year = self.Year[maskKeep]
        self.Status = self.Status[maskKeep]
        self.PA = self.PA[maskKeep]
        self.nWestData = len(self.PA)
        self.Random = self.Random[maskKeep]

    def writeWestToFile(self, westlocationFname):
        """
        # Write result table to file
        """
        # create new structured array with columns of different types
        structured = np.empty((self.nWestData,), dtype=[('zone', 'i8'), ('nest', 'i8'), 
                    ('nestid', 'U8'), ('masternest', 'i8'), ('x', np.float),
                    ('y', np.float), ('year', 'i8'), ('status', 'i8'), 
                    ('pa', 'i8'), ('random', 'i8')])
        # copy data over
        structured['zone'] = self.Zone
        structured['nest'] = self.Nest
        structured['nestid'] = self.NestID
        structured['masternest'] = self.MasterNest
        structured['y'] = self.Y
        structured['x'] = self.X
        structured['year'] = self.Year
        structured['status'] = self.Status
        structured['pa'] = self.PA
        structured['random'] = self.Random
        fmts = ['%i', '%i', '%s', '%i', '%.2f', '%.2f', '%i', '%i', '%i', '%i']
        np.savetxt(westlocationFname, structured, comments = '', delimiter = ',', fmt = fmts,
                    header='zone, nest, NestID, MasterNest, x, y, year, status, pa, random')


def main():
    # set paths to scripts and data
    inputDataPath = os.path.join(os.getenv('KIWIPROJDIR', default='.'), 'goshawk_data')

    ## set Data names
    westFname = os.path.join(inputDataPath, 'westNoDetectMarch2019.csv')
    knownFname = os.path.join(inputDataPath, 'knownWestMarch2019.csv') 
#    knownFname = os.path.join(inputDataPath, 'knownEWNestsAllYears.csv') 
#    westlocationFname = os.path.join(inputDataPath, 'westLocationData_March2019.csv')
    westlocationFname = os.path.join(inputDataPath, 'westLocationData_2.csv')
    
    locdata = LocData(westFname, knownFname, westlocationFname)


if __name__ == '__main__':
    main()
