#!/usr/bin/env python

import os
import numpy as np
#from numba import jit
#import pylab as P
import pandas as pd


def decodeBytes(byteArray, nArray):
    """
    ## loop to decode byte to string
    """
    strArray = str(byteArray[0], 'utf-8')
    for i in range(1, nArray):
        strArr_i = str(byteArray[i], 'utf-8')
        strArray = np.append(strArray, strArr_i)
    return(strArray)


def distFX(x1, y1, x2, y2):
    """
    calc distance matrix between 2 sets of points
    resulting matrix dimensions = (len(x1), len(x2))
    """
    deltaX = np.add.outer(x1, -x2)
    deltaX_sq = deltaX**2
    deltaY = np.add.outer(y1, -y2)
    deltaY_sq = deltaY**2
    dist_2D = np.sqrt(deltaX_sq + deltaY_sq)
    return dist_2D


class LocData(object):
    """
    read and get data
    """
    def __init__(self, knownFname, finalFname):
        #########################################
        ## run functions  #######################

        self.readdata(knownFname)
        self.makeNewNestID()
        self.removeDups()
        self.removeCloseRandom()
        self.getUnknownTheta()
        self.writePandasFile(finalFname)

        ## end running functions  ###############
        #########################################

    def readdata(self, knownFname):
        """
        ## read in data for known nests
        """
        self.locDat = pd.read_csv(knownFname, delimiter=',')
        self.x = np.array(self.locDat[['x']]).flatten()
        self.y = np.array(self.locDat[['y']]).flatten()
        self.zone = np.array(self.locDat[['zone']]).flatten()
        self.nest = np.array(self.locDat[['Nest']]).flatten()
        self.NestID = np.array(self.locDat[['NestID']]).flatten()
        self.masterNest = np.array(self.locDat[['MasterNest']]).flatten()
        self.year = np.array(self.locDat[['year']]).flatten()
        self.pa = np.array(self.locDat[['pa']]).flatten()
        self.random = np.array(self.locDat[['random']]).flatten()

        print('before n', len(self.x))

#        print('locdat', self.locDat[:10, :7])


    def makeNewNestID(self):
        """
        ## MAKE NEW NEST ID
        """
        seqDat = np.arange(len(self.x), dtype = int)
        self.newNest = self.masterNest.copy()
        ## MAKE WEST NESTS FIRST
        maskzone2 = self.zone == 2
        maskrandom1 = self.random == 1
        maskzone2rand1 = maskzone2 & maskrandom1
        ## WEST RANDOM NESTS
        self.newNest[maskzone2rand1] = seqDat[maskzone2rand1]
        ## UPDATE EAST NESTS
        maskzone1 = self.zone == 1
        maskrandom0 = self.random == 0
        maskzone1rand0 = maskzone1 & maskrandom0
        # KNOWN EAST NESTS
        self.newNest[maskzone1rand0] = self.masterNest[maskzone1rand0] + 1000
        # RANDOM EAST NESTS
        maskzone1rand1 = maskzone1 & maskrandom1
        self.newNest[maskzone1rand1] = seqDat[maskzone1rand1] + 1000
#        ## DEBUGGING
#        for i in range(len(self.x)):
#            print('zone', self.zone[i],
#                'random', self.random[i],
#                'nest', self.nest[i],
#                'NestID', self.NestID[i],
#                'masterNest', self.masterNest[i],
#                'newNest', self.newNest[i])


    def removeDups(self):
        """
        ## NEED TO REMOVE DUPLICATES FOR NEST_I
        """
        pa1Mask = self.pa == 1
        keepMask = np.zeros(len(self.x), dtype = bool)
        ## loop unique nests to get 1 entry of location data for each nest-year combo
        uNestsArr = np.unique(self.newNest)
        nUNests = len(uNestsArr)
        for i in range(nUNests):
            nestMask_i = self.newNest == uNestsArr[i]
            uYear_i = np.unique(self.year[nestMask_i])
            for j in range(len(uYear_i)):
                yrMask = self.year == uYear_i[j]
                nestYearMask = yrMask & nestMask_i
                ## number of year entries for nest i and year j
                nYrDat = np.sum(nestYearMask)
                ## if more than one entry for year j
                if nYrDat > 1:
                    pa_ij = self.pa[nestYearMask]
                    # if only 1 positive for year j, keep it
                    if (np.sum(pa_ij) == 1):
                        nestYearPA1Mask = pa1Mask & nestYearMask
                        keepMask[nestYearPA1Mask] = True
                    # if more than 1 positive for year j, k
                    elif (np.sum(pa_ij) > 1):
                        nestYearPA1Mask = pa1Mask & nestYearMask
                        lenPA1 = np.sum(nestYearPA1Mask)
                        fillArr = np.zeros(lenPA1, dtype = bool)
                        fillArr[0] = True
                        keepMask[nestYearPA1Mask] = fillArr
                    elif (np.sum(pa_ij) == 0):
                        fillArr = np.zeros(nYrDat, dtype = bool)
                        fillArr[0] = True
                        keepMask[nestYearMask] = fillArr
                elif nYrDat == 1:
                    keepMask[nestYearMask] = True

        indexNames = self.locDat[~keepMask].index
        self.locDat.drop(indexNames , inplace=True)
        
        self.newNest = self.newNest[keepMask]
        self.x = self.x[keepMask]
        self.y = self.y[keepMask]
        self.zone = self.zone[keepMask]
        self.nest = self.nest[keepMask]
        self.NestID = self.NestID[keepMask]
        self.masterNest = self.masterNest[keepMask]
        self.year = self.year[keepMask]
        self.pa = self.pa[keepMask]
        self.random = self.random[keepMask]

        print(' after n', len(self.x))
            
    def removeCloseRandom(self):
        """
        remove random points close to newnests
        """
        keepMask = np.zeros(len(self.x), dtype=bool)
        keepMask[self.random == 0] = True
        uRandNest = np.unique(self.newNest[self.random == 1])
        for i in range(len(uRandNest)):
            maskRandNest = self.newNest == uRandNest[i]
            year_i = self.year[maskRandNest]
            maskYear = self.year == year_i
            yearKeepMask = maskYear & keepMask
            x_i = self.x[yearKeepMask]
            y_i = self.y[yearKeepMask]
            dist_i = distFX(self.x[maskRandNest], self.y[maskRandNest], x_i, y_i)
            if np.min(dist_i) >= 1000.0:
                keepMask[maskRandNest] = True

        indexNames = self.locDat[~keepMask].index
        self.locDat.drop(indexNames , inplace=True)
        
        self.newNest = self.newNest[keepMask]
        self.x = self.x[keepMask]
        self.y = self.y[keepMask]
        self.zone = self.zone[keepMask]
        self.nest = self.nest[keepMask]
        self.NestID = self.NestID[keepMask]
        self.masterNest = self.masterNest[keepMask]
        self.year = self.year[keepMask]
        self.pa = self.pa[keepMask]
        self.random = self.random[keepMask]

        print(' after remove close random', len(self.x), 'shape', self.locDat.shape)
            
    def getUnknownTheta(self):
        unTH = np.zeros(len(self.x), dtype = int)
        prevTH = np.zeros(len(self.x), dtype = int)
        uNest = np.unique(self.newNest)
        for i in range(len(uNest)):
            maskNest = self.newNest == uNest[i]
            year_Arr = self.year[maskNest]
            ## IF SINGLE YEAR FOR NEST i
            if (len(year_Arr) == 1):
                ## CHANGE ONLY NON-RANDOM LOCS 
                if (self.random[maskNest] == 0):
                    unTH[maskNest] = 1
                    prevTH[maskNest] = np.random.binomial(1, 0.2)
                ## GO BACK TO TOP OF LOOP AND PROCESS NEXT NEST
                continue

            yearSort = np.sort(year_Arr)
            ## first year
            firstYear = yearSort[0]
            maskNestFirstYear = maskNest & (self.year == firstYear)
            unTH[maskNestFirstYear] = 1
            prevTH[maskNestFirstYear] = np.random.binomial(1, 0.2)

#            print('1111  nest', uNest[i], 'year_Arr', year_Arr, len(year_Arr))

            for j in range(1, len(yearSort)):
                year_j = yearSort[j]
                maskYrSort = yearSort == year_j
                if np.sum(maskYrSort) > 1:
                    print("Error, have more than one year")
                    print('Nest', uNest[i], 'year', year_j, 'maskYrSort', maskYrSort,
                        'sum mask', np.sum(maskYrSort))
                     
                maskAllYear = self.year == year_j
                # for indexing the unTH and prevTH
                maskNestAllYear = maskAllYear & maskNest
                # if no data in previous year 
                if (yearSort[j] - yearSort[j-1]) > 1:
                    unTH[maskNestAllYear] = 1
                    prevTH[maskNestAllYear] = np.random.binomial(1, 0.2)
                ## or, if data in previous year
                else:
                    unTH[maskNestAllYear] = 0
                    maskPrevYear = (self.year == yearSort[j-1])
                    maskNestPrevYear = maskPrevYear & maskNest
                    prevTH[maskNestAllYear] = self.pa[maskNestPrevYear]

        self.locDat.loc[:, 'prevPA'] = prevTH
        self.locDat.loc[:, 'unTH'] = unTH




    def writePandasFile(self, finalFname):
        """
        write pandas df to directory
        """
        self.locDat.loc[:, 'nid'] = self.newNest
        self.locDat.to_csv(finalFname, sep=',', header=True, index_label = 'did')



def main():
    # set paths to scripts and data
    inputDataPath = os.path.join(os.getenv('KIWIPROJDIR', default='.'), 'goshawk_data')

    ## set Data names
    knownFname = os.path.join(inputDataPath, 'Goshawk_MasterEW.csv') 
#    knownFname = os.path.join(inputDataPath, 'knownEWNestsAllYears.csv') 
#    westlocationFname = os.path.join(inputDataPath, 'westLocationData_March2019.csv')
    finalFname = os.path.join(inputDataPath, 'gosData_Final.csv')
    
    locdata = LocData(knownFname, finalFname)


if __name__ == '__main__':
    main()
