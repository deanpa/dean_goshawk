#######
############  

#########   Goshawks Uni-variable analysis
rm(list = ls())



library(MASS)
library(mvtnorm)
                 ###########   Functions    #################
          
b.update  <- function(y,x){    
	tb <- inv.logit(x%*%bp + zx*zp + yx%*%yp)    
    for(i in 1:bcov)
       {	
	       bs <- bp
	       bs[i] <- rnorm(1,bp[i],bjump[i])
###	       tb.s <- inv.logit(x%*%bs)

           tb.s <- inv.logit(x%*%bs + zx*zp + yx%*%yp)

	       pnow <- sum(dbinom(y,1,tb,log=T)) + dnorm(bp[i],0,5,log=T)
	       pnew <- sum(dbinom(y,1,tb.s,log=T)) + dnorm(bs[i],0,5,log=T)
	       r <- exp(pnew - pnow)
           z <- runif(1,0,1)
           if(r>z)
              {
	              bp[i] <- bs[i]
	              tb <- tb.s
              }
       }
  return(bp)
   } 


b.updateAll  <- function(y,x){    
    tb <- inv.logit(x%*%bp + zx*zp + yx%*%yp)
           nbcov = dim(x)[2]    
           bs <- rnorm(nbcov, bp, bjump)
           tb.s <- inv.logit(x%*%bs + zx*zp + yx%*%yp)
           pnow <- sum(dbinom(y,1,tb,log=T)) + sum(dnorm(bp,0,5,log=T))
           pnew <- sum(dbinom(y,1,tb.s,log=T)) + sum(dnorm(bs,0,5,log=T))
           r <- exp(pnew - pnow)
           z <- runif(1,0,1)
           if(r>z)
              {
                  bp <- bs
                  tb <- tb.s
              }
       
  return(bp)
   } 

  
z.update  <- function(y){     
	  zs <- rnorm(zcov,zp,.45)
#	  zs1 <- rnorm(zcov,zp,.35)
#	  zs <- zs1 - mean(zs1)
	  t.s <- inv.logit(bx%*%bp + zx*zs + yx%*%yp)  
	  pnow <- sum(dbinom(y,1,t,log=T)) + sum(dnorm(zp,0,1.5,log=T))
	  pnew <- sum(dbinom(y,1,t.s,log=T)) + sum(dnorm(zs,0,1.5,log=T))
#	  pnow <- sum(dbinom(y,1,t,log=T)) + sum(dcauchy(zp,0,10,log=T))
#	  pnew <- sum(dbinom(y,1,t.s,log=T)) + sum(dcauchy(zs,0,10,log=T))
	  r <- exp(pnew - pnow)
      z <- runif(1,0,1)
      if(r>z)
        {
	        zp <- zs
        }
     return(zp)
   } 


## Year update for years 2000 - 2017; 1999 is intercept
y.update  <- function(y){    
	ys <- rnorm(ycov,yp,.4)
#	ys1 <- rnorm(ycov,yp,.4)
#	ys <- ys1 - mean(ys1)
	t.s <- inv.logit(bx%*%bp + zx*zp + yx%*%ys)  
	  pnow <- sum(dbinom(y,1,t,log=T)) + sum(dnorm(yp,0,1.5,log=T))
	  pnew <- sum(dbinom(y,1,t.s,log=T)) + sum(dnorm(ys,0,1.5,log=T))
#	  pnow <- sum(dbinom(y,1,t,log=T)) + sum(dcauchy(yp,0,10,log=T))
#	  pnew <- sum(dbinom(y,1,t.s,log=T)) + sum(dcauchy(ys,0,10,log=T))
	r <- exp(pnew - pnow)
    z <- runif(1,0,1)
    if(r>z)
      {
	      yp <- ys
       }
  return(yp)
   }

        #########   update theta t-1 where unknown and not random
thLag.update <- function(y){
  lth_1.s <- ifelse(lth_1[unTH==1]==0,1,0)
  bx.s[unTH==1,3] <- lth_1.s
  mu.s <- inv.logit(bx.s[unTH==1,]%*%bp + zx[unTH==1]*zp + yx[unTH==1,]%*%yp)
  pnow <- dbinom(y[unTH==1],1,t[unTH==1],log=T) 
  pnew <- dbinom(y[unTH==1],1,mu.s,log=T) 
  r <- exp(pnew - pnow)
  z <- runif(length(lth_1.s),0,1)
  lth_1[unTH==1][r>z] <- lth_1.s[r>z]
  return(lth_1[unTH==1])
  }


logit <- function(x){log(x/(1-x))}
inv.logit <- function(x){1/(1 + exp(-x))}      
                 
                 #############   Finish Functions   ###########

savePath <- "/nesi/project/landcare00074/goshawk/Results"              

gos <- read.table("goshawk_data/GosData_Finalv1.csv",sep=",",header=T)
pntID<- seq(1,dim(gos)[1])
year <- gos$year
uyear <- sort(unique(year))
pa <- gos$pa
xdat <- gos$x
ydat <- gos$y
zone <- gos$zone
uzone <- sort(unique(zone))
#nid <- gos$newNest
unTH = gos$unTH
lth_1 = gos$prevPA

#                  ############### id data with unknown theta at t-1.
#unTH <- rep(0,length(pntID))
#thseq <- rep(0,length(pntID))
#for(i in 1:length(pntID))
#    {
#	    unTH[i] <- ifelse(year[pntID[i]]==min(year[nid == nid[pntID[i]]]), 1,
#                   ifelse(year[pntID[i]]-year[pntID[i-1]]>1, 1, 0))
#	    thseq[i] <- ifelse(unTH[i]==0,(i-1),0)
#    }

         #############   
nData <- length(pa)

bcov <- 3
xs <- matrix(0,bcov,length(pa))
bscale <- c(10,2.5,2.5)

bjump <- c(.45,.45,.45)   
zcov <- 1
ycov <- 18

##################################
########## User modify

#rm(uniCov)
#uniCov = gos$UpHard.1000
#uniCov = gos$Wet.Con.500
#uniCov = gos$LowHard.500
#uniCov = gos$Regen.500
#uniCov = gos$Asp.Bals.500 + gos$Asp.Bir.500
#uniCov = gos$primrds.500
#uniCov = gos$Dry.Conif.200
#uniCov = gos$Open.200 + gos$Shrub.200 + gos$Oak.DryCon.200 + gos$JackPin.Oak.200
uniCov = gos$secrds.200



ngibbs <- 3000   #3000
bgibbs <- matrix(0,ngibbs,bcov)              # beta data and initial parameters 
sgibbs <- rep(0,ngibbs)  
zgibbs <- matrix(0,ngibbs,zcov) 
ygibbs <- matrix(0,ngibbs,ycov)              # year data and parameters
th_1DICmat <- matrix(0,ngibbs,length(pa))

thinrate<- 30    #30
burnin <- 1500 #4000
keepseq <- seq((burnin+1),ngibbs*thinrate + burnin,by=thinrate)

##########
##################################

bp <- c(-1.0,.5,-.51) 

zxs <- rep(0,length(pa))         ##### zone data and initial parameters         
  zp <- .05 

yxs <- matrix(0,ycov,length(pa)) 
  yp <- runif(ycov, -.1, .1)   

# zone effect for West; zone = 2
zx <- ifelse(zone==2,1,0) 

#year effect will sum to zero; for regularisation of priors
yx <- matrix(0,length(pa),ycov)
for(i in 1:ycov)
    {
	    yx[,i] <- ifelse((year-1999)==i,1,0) 
    } 
    
bxx <- scale(uniCov)/2
bx <- cbind(rep(1,length(pa)),bxx,rep(0,length(pa)))
bx[,3] <- lth_1
bx.s <- bx
t <- inv.logit(bx%*%bp + zx*zp + yx%*%yp)

  ########  
  #########################   Gibbs sampler
  ########  
  cc <- 1
   for(g in 1:(ngibbs*thinrate+burnin)){	
	                        ######   update parameters
bp <- b.updateAll(pa,bx)
t <- inv.logit(bx%*%bp + zx*zp + yx%*%yp)

zp <- z.update(pa)   
t <- inv.logit(bx%*%bp + zx*zp + yx%*%yp)

yp <- y.update(pa)   
t <- inv.logit(bx%*%bp + zx*zp + yx%*%yp)

lth_1[unTH==1] <- thLag.update(pa) 
bx[unTH==1,3] <- lth_1[unTH==1]
t <- inv.logit(bx%*%bp + zx*zp + yx%*%yp)
 
if(g%in%keepseq){ 
  bgibbs[cc,] <- bp
  zgibbs[cc] <- zp  
  ygibbs[cc,] <- yp 
  th_1DICmat[cc,] <- lth_1
  cc <- cc + 1 
  }
}
#########################          
 keep <- seq(1,(cc-1),by=1)              #thin predictions
 outpar <- matrix(NA,nrow=22,ncol=3)
 for(i in 1:3)                     
     {
	     outpar[i,1:3]<-c(quantile(bgibbs[keep,i],c(0.5,0.05,0.95)))
     }
    outpar[4,1:3]<-c(quantile(zgibbs[keep],c(0.5,0.05,0.95)))
 for(i in 1:18)                     
     {
	     outpar[i+4,1:3]<-c(quantile(ygibbs[keep,i],c(0.5,0.05,0.95)))
     }
  dimnames(outpar)[2] <- list(c("Median","5% CI","95% CI"))
  dimnames(outpar)[1] <- list(c("Intercept","Hwd500",  "theta t-1","zone 2", 
            "year 2000","year 2001","year 2002", "year 2003","year 2004", 
            "year 2005","year 2006", "year 2007", "year 2008","year 2009",
            "year 2010","year 2011","year 2012", "year 2013","year 2014",
            "year 2015", "year 2016","year 2017"))
  round(outpar,3)
  ngibbs

# par(mfrow=c(2,2),pty="s") 
#  plot(bgibbs[keep,1],type="l")   
#  plot(bgibbs[keep,2],type="l") 
#  plot(bgibbs[keep,3],type="l")   
#  plot((t),jitter(pa))   


      
# par(mfrow=c(1,1),pty="s") 
#  plot(zgibbs[keep],type="l")   

      
# par(mfrow=c(3,4),pty="s") 
#  plot(ygibbs[keep,1],type="l")   
#  plot(ygibbs[keep,2],type="l") 
#  plot(ygibbs[keep,3],type="l")   
#  plot(ygibbs[keep,4],type="l") 
#  plot(ygibbs[keep,5],type="l")   
#  plot(ygibbs[keep,6],type="l") 
#  plot(ygibbs[keep,7],type="l")   
#  plot(ygibbs[keep,8],type="l") 
#  plot(ygibbs[keep,9],type="l")   
#  plot(ygibbs[keep,10],type="l")   
#  plot(ygibbs[keep,11],type="l")   
#  plot(ygibbs[keep,12],type="l") 


# par(mfrow=c(2,3),pty="s") 
#  plot(ygibbs[keep,13],type="l")   
#  plot(ygibbs[keep,14],type="l") 
#  plot(ygibbs[keep,15],type="l")   
#  plot(ygibbs[keep,16],type="l") 
#  plot(ygibbs[keep,17],type="l")   
#  plot(ygibbs[keep,18],type="l") 
#  
  
  #thDat <- as.data.frame(cbind(lth,lth_1))
  #write.table(thDat,"thDat.csv",sep=",",row.names=F)
#   par(mfrow=c(2,2),pty="s") 
#  acf(bgibbs[keep,1], lag.max = NULL,type = "correlation",plot = TRUE,)
#  acf(bgibbs[keep,2], lag.max = NULL,type = "correlation",plot = TRUE,)
#  acf(bgibbs[keep,3], lag.max = NULL,type = "correlation",plot = TRUE,)
#  acf(zgibbs[keep,1], lag.max = NULL,type = "correlation",plot = TRUE,)
#  
  
  
  
     ######################################
            #############################################  Calculate DIC
            ######################################



d1 <- rep(0,length(keep))
xbeta <- bx
for(i in 1:length(keep))
    {
	    xbeta[,3] <- th_1DICmat[keep[i],]
	    muDIC <- (xbeta%*%bgibbs[keep[i],] + zx*zgibbs[keep[i]] + 
                yx%*%ygibbs[keep[i],])
	    d1[i] <- sum(dbinom(pa,1,inv.logit(muDIC),log=T)) + 
	             sum(dnorm(bgibbs[keep[i],],0,5,log=T)) +
	             sum(dnorm(zgibbs[keep[i]],0,1.5,log=T)) +
	             sum(dnorm(ygibbs[keep[i],],0,1.5,log=T)) 
    }
dbar <- mean(-2*d1)

meanb<-apply(bgibbs[keep,],2,mean)
meanz <- mean(zgibbs[keep])
meany <- apply(ygibbs[keep,],2,mean)
lth_1sum <- apply(th_1DICmat[keep,],2,sum)

lth_1med <- ifelse(lth_1sum >= (ngibbs/2),1,0)

xbeta[,3] <- lth_1med
muDIC <- xbeta%*%meanb + zx*meanz + yx%*%meany

  dtbar <- sum(dbinom(pa,1,inv.logit(muDIC),log=T)) + 
	       sum(dnorm(meanb,0,5,log=T)) +
	       sum(dnorm(meanz,0,1.5,log=T)) +
	       sum(dnorm(meany,0,1.5,log=T))
dtbar2 <- -2*dtbar	       
DIC <- (2*dbar) - dtbar2
DIC

pd <- dbar - dtbar2
pd

                                

 ########
#  par(mfrow=c(2,2),pty="s")   
#  msig <- cumsum(bgibbs[keep,1])/seq(1,length(keep))
#  plot(seq(1,length(keep)),msig,type="l")   
 
                         
#  msig <- cumsum(bgibbs[keep,2])/seq(1,length(keep))
#  plot(seq(1,length(keep)),msig,type="l")   
# 
  
#  msig <- cumsum(bgibbs[keep,3])/seq(1,length(keep))
#  plot(seq(1,length(keep)),msig,type="l")   
 
  
  
  
######################################
######################################
######  GET PERCENT COVER

#covPercent = uniCov
### Un-occupied
#mean(covPercent[pa==0])  
#sd(covPercent[pa==0])
#max(covPercent[pa==0])
  
### Occupied
#mean(covPercent[pa==1])  
#sd(covPercent[pa==1])
#max(covPercent[pa==1])
  
  
# save.image("savePath/upHard200.RDATA")

