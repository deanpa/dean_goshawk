#!/usr/bin/env python

import os
import numpy as np
#from numba import jit
#import pylab as P
#import pandas as pd


def decodeBytes(byteArray, nArray):
    """
    ## loop to decode byte to string
    """
    strArray = str(byteArray[0], 'utf-8')
    for i in range(1, nArray):
        strArr_i = str(byteArray[i], 'utf-8')
        strArray = np.append(strArray, strArr_i)
    return(strArray)


def distFX(x1, y1, x2, y2):
    """
    calc distance matrix between 2 sets of points
    resulting matrix dimensions = (len(x1), len(x2))
    """
    deltaX = np.add.outer(x1, -x2)
    deltaX_sq = deltaX**2
    deltaY = np.add.outer(y1, -y2)
    deltaY_sq = deltaY**2
    dist_2D = np.sqrt(deltaX_sq + deltaY_sq)
    return dist_2D


class LocData(object):
    """
    read and get data
    """
    def __init__(self, eastFname, westFname, westEastFname):

        ## run functions  #######################

        self.readEastNests(eastFname)
        self.removeDups()
        self.readWestNests(westFname)
        self.writeToFile(westEastFname)
        ## end running functions  ###############


    def readEastNests(self, eastFname):
        """
        ## read in data for known nests
        """
        eastNestDat = np.genfromtxt(eastFname,  delimiter=',', names=True,
            dtype=['i8', 'i8', 'S10', 'i8', 'f8', 'f8', 'i8', 'i8', 'i8', 'i8'])
        self.eastZone = eastNestDat['zone']
        self.eastNest = eastNestDat['nest']
        self.eastNestID = eastNestDat['NestID']
        self.eastMasterNest = eastNestDat['MasterNest']
        self.eastY = eastNestDat['y']
        self.eastX = eastNestDat['x']
        self.eastYear = eastNestDat['year']
        self.eastStatus = eastNestDat['status']
        self.eastPA = eastNestDat['pa']
        self.eastRandom = eastNestDat['random']
        self.neast = len(self.eastX)

        self.eastMasterID = np.zeros(self.neast, dtype = int)
        for i in range(self.neast):
            if self.eastRandom[i] == 1:
                self.eastMasterID[i] = i + 100
        
        ## convert bytes to string and format Dates
#        self.knownNestID = decodeBytes(self.knownNestID, self.nKnown)


    def removeDups(self):
        """
        remove repeat entries for nests with > 1 entry per year
        """
        pres1Mask = self.eastPA == 1
        remMask = np.zeros(self.neast, dtype = bool)
        uNests = np.unique(self.eastMasterNest)
        nUNests = len(uNests)
        for i in range(nUNests):
            nest_i = uNests[i]
            nestMask = self.eastMasterNest == nest_i
            uYears_i = np.unique(self.eastYear[nestMask])
            nUYears_i = len(uYears_i)

            for j in range(nUYears_i):
                year_j = uYears_i[j]
                yearMask = self.eastYear == year_j
                nestYearMask = yearMask & nestMask

                if nest_i == 0:
                    print('nest', nest_i, 'eastMasterNest', self.eastMasterNest[nestYearMask],
                        'years', self.eastYear[nestYearMask], 
                        'sum mask', np.sum(nestYearMask)) 

                if np.sum(nestYearMask) > 1:
                    ## make all entries to be removed; make one to keep below
                    remMask[nestYearMask] = True
                    pa_j = self.eastPA[nestYearMask]
                    nPres = np.sum(pa_j)
                    ## IF 1 or MORE PRESENCE, then keep first                                        
                    if nPres > 0:
                        paNestYearMask = pres1Mask & nestYearMask
                        datumRemoved = False
                        ## LOOP THRU ALL ENTRIES
                        for k in range(self.neast):
                            if paNestYearMask[k] & (not datumRemoved):
                                remMask[k] = False
                                datumRemoved = True

                        print('2nd Filter more than 1 pres, year =', year_j, 
                            'nests = ', nest_i, 'pa_j', pa_j,
                            'remmask', remMask[nestYearMask])

                    ## IF ALL ARE ABSENT
                    elif nPres == 0:
                        datumRemoved = False
                        ## LOOP THRU ALL ENTRIES
                        for k in range(self.neast):
                            if nestYearMask[k] & (not datumRemoved):
                                remMask[k] = False
                                datumRemoved = True

                        print('2nd Filter more than 1 pres, year =', year_j, 
                            'nests = ', nest_i, 'pa_j', pa_j,
                            'remmask', remMask[nestYearMask])
        ## REMOVE ENTRIES FROM ARRAYS
        self.eastZone = self.eastZone[~remMask]
        self.eastStatus = self.eastStatus[~remMask]
        self.eastNest = self.eastNest[~remMask]
        self.eastNestID = self.eastNestID[~remMask]
        self.eastMasterNest = self.eastMasterNest[~remMask]
        self.eastX = self.eastX[~remMask]
        self.eastY = self.eastY[~remMask]
        self.eastPA = self.eastPA[~remMask]
        self.eastYear = self.eastYear[~remMask]
        self.eastRandom = self.eastRandom[~remMask]
        self.neast = len(self.eastPA)

        print('n east obs', self.neast)


    def readWestNests(self, westFname):
        """
        ## read in data for known nests
        """
        westNestDat = np.genfromtxt(westFname,  delimiter=',', names=True,
            dtype=['i8', 'i8', 'S10', 'i8', 'f8', 'f8', 'i8', 'i8', 'i8', 'i8'])
        self.westZone = westNestDat['zone'] + 2
        self.westNest = westNestDat['nest']
        self.westNestID = westNestDat['NestID']
        self.westMasterNest = westNestDat['MasterNest']
        self.westY = westNestDat['y']
        self.westX = westNestDat['x']
        self.westYear = westNestDat['year']
        self.westStatus = westNestDat['status']
        self.westPA = westNestDat['pa']
        self.westRandom = westNestDat['random']
        self.nwest = len(self.westX)

        self.westMasterID = np.zeros(self.nwest, dtype = int)
        for i in range(self.nwest):
            if self.westRandom[i] == 1:
                self.westMasterID[i] = i + 5000
            elif self.westRandom[i] == 0:
                self.westMasterID[i] = self.westMasterNest[i] + 3000
        

        print('n west obs', self.nwest)


    def writeToFile(self, westEastFname):
        """
        # Write result table to file
        """
        # create new structured array with columns of different types
        ndat = self.neast + self.nwest
        structured = np.empty((ndat,), dtype=[('zone', 'i8'), ('nest', 'i8'), 
                    ('nestid', 'U8'), ('masternest', 'i8'), ('x', np.float),
                    ('y', np.float), ('year', 'i8'), ('status', 'i8'), 
                    ('pa', 'i8'), ('random', 'i8')])
        # copy data over
        structured['zone'] = np.append(self.eastZone, self.westZone)
        structured['nest'] = np.append(self.eastNest, self.westNest)
        structured['nestid'] = np.append(self.eastNestID, self.westNestID)
        structured['masternest'] = np.append(self.eastMasterNest, self.westMasterNest)
        structured['y'] = np.append(self.eastY, self.westY)
        structured['x'] = np.append(self.eastX, self.westX)
        structured['year'] = np.append(self.eastYear, self.westYear)
        structured['status'] = np.append(self.eastStatus, self.westStatus)
        structured['pa'] = np.append(self.eastPA, self.westPA)
        structured['random'] = np.append(self.eastRandom, self.westRandom)
        fmts = ['%i', '%i', '%s', '%i', '%.2f', '%.2f', '%i', '%i', '%i', '%i']
        np.savetxt(westEastFname, structured, comments = '', delimiter = ',', fmt = fmts,
                    header='zone, nest, NestID, MasterNest, x, y, year, status, pa, random')


def main():
    # set paths to scripts and data
    inputDataPath = os.path.join(os.getenv('KIWIPROJDIR', default='.'), 'goshawk_data')

    ## set Data names
    westFname = os.path.join(inputDataPath, 'westLocationData_2.csv') 
    eastFname = os.path.join(inputDataPath, 'eastLocationData.csv')
    westEastFname = os.path.join(inputDataPath, 'westEastData.csv')
    locdata = LocData(eastFname, westFname, westEastFname)


if __name__ == '__main__':
    main()

